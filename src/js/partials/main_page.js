$(document).ready(function () {
    InitLazyLoad();
    InitSmoothScroll();
    FormMask();
    FormVisibility();
    ButtonFormCheckInformation();
    FormSend();
    ReverseEmail();
    InitShowMoreButton();
    InitGalleryButtons();
    MobileMenu();
    VideoContainer();
    $(document).foundation();
});

$(window).bind("load", function(){
    $("img.lazy").trigger("sporty");
    InitVideoInModals();
    InitMapsAPI();
    InitVkAPI();
    InitFbAPI();
});

function InitLazyLoad(){
    $("img.lazy").lazyload({
        effect: "fadeIn",
        effectTime: 500,
        event: "sporty"
    })
}

function VideoContainer(){
    $("#pervorobotVideoContainer").hide();
    $('#pervorobotVideoContainer').YTPlayer({
        fitToBackground: true,
        videoId: 'qd5m7HFrfRc',
        callback: function() {
            var player = $('#pervorobotVideoContainer').data('ytPlayer').player;
            player.setPlaybackQuality('hd720');
            var buffer = 0;

            var interval = setInterval(
                function () {
                    buffer = player.getVideoLoadedFraction();
                    if (buffer >= 0.3)
                    {
                        player.seekTo(0);
                        $("#pervorobotVideoContainer").show();
                        clearInterval(interval);
                    }
                }, 1000);
        }
    })
}

function InitVideoInModals(){
    $("#videoRobotsAttack").find("iframe").attr("src", "http://www.youtube-nocookie.com/embed/E7_WOdMKC2o?rel=0");
    $("#videoGTRK").find("iframe").attr("src", "http://www.youtube-nocookie.com/embed/5YabPI-oytk?rel=0");
    $("#videoCompetitions").find("iframe").attr("src", "http://www.youtube-nocookie.com/embed/nLKEXjgbAwE?rel=0");
}

function InitMapsAPI(){
    var script = document.createElement('script');
    script.src = "//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU";
    document.body.appendChild(script);

    script.onload = function(){
        InitMap()
    }
}

function InitMap(){
    var myMap;
    ymaps.ready(init);
    function init () {
        myMap = new ymaps.Map('map', {
            center: [56.625253, 47.892993],
            zoom: 17,
            height: 373
        });
        myMap.controls
            .add('zoomControl', { left: 5, top: 5 })
            .add('typeSelector')
            .add('mapTools', { left: 35, top: 5 });

        var myPlacemark = new ymaps.Placemark([56.625176, 47.895439], {}, {
            iconImageHref: '/upload/pervorobot_files/images/ymap_balloon.png',
            iconImageSize: [53, 83],
            iconImageOffset: [-26, -81]
        });

        myMap.geoObjects.add(myPlacemark);
    }
}

function InitFbAPI(){
    var script = document.createElement('script');
    script.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
    document.body.appendChild(script);
}

function InitVkAPI(){
    var script = document.createElement('script');
    script.src = "http://vk.com/js/api/openapi.js";
    document.body.appendChild(script);

    script.onload = function(){
        InitVK()
    }
}

function InitVK(){
    var vkGroupContainer = $("#vk");
    PlaceVKVidget(vkGroupContainer);
}

function PlaceVKVidget(vkGroupContainer){
    vkGroupContainer.empty();
    VK.Widgets.Group("vk", {mode: 0, width: 300, height: 350}, 112243247);
}

function InitSmoothScroll(){
    $('.smooth').smoothScroll({offset: -100});
}

function FormMask() {
    $(function(){
        $("input[data-type=phone]").inputmask("+7 (999) 999-99-99", {
            "clearIncomplete": true
        });

        $("input[data-type=email]").inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9А-Яа-яЁёA-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    cardinality: 1,
                    casing: "lower"
                }
            }
        });

        $("input[data-type=name]").inputmask({
            mask: "*{1,50}",
            placeholder: "",
            greedy: false,
            definitions: {
                '*': {
                    validator: "[A-Za-zА-Яа-яЁё -]"
                }
            }
        })
    })
}

function FormVisibility(){
    $('.competitions').change(function (){

        $('#user_quantity').find('option').each(function(){
            $(this).show();
            $('select option[value="default"]').prop('selected', true);
            $('#user_quantity option[value="default"]').css('display', 'none');
        });
        $('.users').find('.user').each(function(){
            $(this).addClass('hidden');
        });
        $('.head').addClass('hidden');

        var minParticipantsQuantity = $("#competitions option:selected").data('min-participants');
        var maxParticipantsQuantity = $("#competitions option:selected").data('max-participants');
        if ((minParticipantsQuantity == 1) && (maxParticipantsQuantity == 2)){
            $('#user_quantity option[value="3"]').css('display', 'none');
            $('#user_quantity option[value="4"]').css('display', 'none');
        }
        if ((minParticipantsQuantity == 2) && (maxParticipantsQuantity == 4)){
            $('#user_quantity option[value="1"]').css('display', 'none');
        }

        $('.user_quantity').removeClass('hidden').change(function (){

            $('#orderPassForm').find('.user').each(function(){
                $(this).addClass('hidden');
            });

            var userQuantity = $('#user_quantity').val();
            if (userQuantity == '1'){
                $('#user_1').removeClass('hidden');
            }
            if (userQuantity == '2'){
                $('#user_1').removeClass('hidden');
                $('#user_2').removeClass('hidden');
            }
            if (userQuantity == '3'){
                $('#user_1').removeClass('hidden');
                $('#user_2').removeClass('hidden');
                $('#user_3').removeClass('hidden');
            }
            if (userQuantity == '4'){
                $('#user_1').removeClass('hidden');
                $('#user_2').removeClass('hidden');
                $('#user_3').removeClass('hidden');
                $('#user_4').removeClass('hidden');
            }
            $('.head').removeClass('hidden');
        });
    });
}

function ButtonFormCheckInformation(){
    $('#form_check_activate').click(function ()
    {
        CheckForEmptyFields($('#orderPassForm'));
    });
    $('#form_check_back').click(function(){
        $('#orderPassForm').find('#input_form_check').addClass('hidden');
        $('#orderPassForm').find('#input_form').removeClass('hidden');
    });
}

function CheckForEmptyFields(form)
{
    var error = false;

    form.find('input').each(function(){
       $(this).focus(function(){
           $(this).removeClass("error")
       })
    });

    form.find('select').each(function(){
        $(this).click(function(){
            $(this).removeClass("error")
        })
    });

    form.find('select').each(function(){
        if ($(this).val() == null)
        {
            $(this).addClass('error');
            error = true;
        }
    });

    form.find('.user').each(function(){
        if (!$(this).hasClass('hidden'))
        {
            $(this).find('input').each(function(){
                if ((this.value == '') && ($(this).attr('data-not-required') != 'true'))
                {
                    $(this).addClass('error');
                    error = true;
                }
            })
        }
    });

    form.find('.head').find('input').each(function(){
        if (this.value == '')
        {
            $(this).addClass('error');
            error = true;
        }
    });

    if (!error)
    {
        form.find('#input_form').addClass('hidden');
        var data = form.serializeArray();
        $.ajax({
                type: 'POST',
                url: '/upload/pervorobot_files/form_check_handler.php',
                data: data,
                success: function (response)
                {
                    form.find('#formResultText').html(response);
                    form.find('#input_form_check').removeClass('hidden');
                },
                error: function (response)
                {
                    console.log(response);
                }
            }
        );
    }
}

function FormSend(){
    $('#form_send').click(function(){
        var data = $("#orderPassForm").serializeArray();
        $.ajax({
                type: 'POST',
                url: '/bitrix/form_submit_ajax_handler.php',
                data: data,
                success: function(response) {
                    if (response == '')
                    {
                        $('#enterSeminarForm').find('h2').html('Ваша заявка отправлена. <br /> Спасибо!')
                        $('#orderPassForm').addClass('hidden');
                        $('#enterSeminarForm').find('#input_form_send').removeClass('hidden');
                    }
                    else
                    {                        
                        console.log(response);
                    }
                }
            }
        )
    })
}

function ReverseEmail() {
    $(".reverse_email").each(function(){
        var eMailAddress = $.trim($(this).text().split('').reverse().join(''));
        $(this).text(eMailAddress).attr('href', 'mailto:' + eMailAddress);
    });
}

function InitShowMoreButton(){
    $(".show_more_button").click(function(event){
        event.preventDefault();

        $($(this).attr('href')).slideToggle(300);
        Foundation.reInit('equalizer');
    })
}

function InitGalleryButtons(){
    var timelineContainer = $("#timelineContainer");
    $("#timelineButtonsList").find(".timeline_button").click(function(event){
        event.preventDefault();

        if (!$(this).parent("li").hasClass("active"))
        {
            $(this).parent("li").siblings().removeClass("active");
            $(this).parent("li").addClass("active");

            timelineContainer.removeClass().addClass("timeline_wrapper");
            timelineContainer.addClass($(this).attr("href").substring(1));

            timelineContainer.find(".timeline_content_wrapper").hide();
            $($(this).attr("href")).fadeIn(500);
        }
    })
}

function MobileMenu(){
    var visible = false;
        $("#topMenuButton").click(function(){
            if (visible){
                $("#topmenuToToggle").removeClass("opened-menu");
                visible = false;
                $(".hamburger").toggleClass("is-active", visible);
                $("html").toggleClass("fixed", visible);
                $("body").toggleClass("fixed", visible);
            }
            else {
                $("#topmenuToToggle").addClass("opened-menu");
                visible = true;
                $(".hamburger").toggleClass("is-active", visible);
                $("html").toggleClass("fixed", visible);
                $("body").toggleClass("fixed", visible);
            }
        });

    $("#topmenuToToggle").find('a').each(function(){
        $(this).click(function(){
            if ($('#topmenuToToggle').hasClass('opened-menu'))
            {
                $('#topMenuButton').click();
            }
        })
    })
}